# Exception request as a Release Manager

Although saying no to a colleague is very hard, your role is to be the release gatekeeper.
This means that whatever code is deployed to gitlab.com is also your responsibility.

It is advisable that you only pick exception requests after getting a release candidate stable enough. This way, if an exception request breaks something, it's easier to track the changes.

## Considerations before accepting an exception request

### What it means for you
Always remind your self that accepting an exception request means that you'll be responsible for releasing and deploying a new version with those changes, which includes waiting for packaging and QA.

### Do you have enough information?
Before accepting an exception request, make sure you:

1. Understand what the changes do and what is their impact
1. Understand the risk of those changes
1. Understand why it's valuable to have them in this release
1. Understand how to fix any possible problem once the changes get to production

If you do not have the answer to any of the above, ask questions before accepting.

### When in doubt, ask
It can happen that the changes made go out of your expertise. Don't be ashamed to ask questions or to reach out to other people involved in the release process for help.

## Rejecting an exception request
It's ok to reject an exception request:
- If you think that the risks are bigger than the value the change adds
- There isn't enough value on the change to make up for the effort on tagging a new release
- If you think you do not have enough information
- There isn't a plan to mitigate the risks
- If you think there's not enough time to prepare an RC or patch release

## Accepting an exception requests:
You should accept an exception request when:
- You understand the impact of the changes, it's valuable to have them and there is a risk mitigation plan
- The changes are already applied in a production patch and are working correctly

## Solving conflicts
Communication can be hard. If you are having troubles explaining yourself, ask for help from other people involved in the release process.
When in doubt, check with [the person coordinating the release](https://about.gitlab.com/team/#maxlazio)
