## Overview

Post-deployment patches make it possible for SREs to apply changes outside of the
normal release cycles. These changes bypass the normal packaging and release
cycle for expediency, when there is community impact on GitLab.com or a security
vulnerability that needs immediate attention.

*Every post deployment patch must have an associated S1 or S2 production incident*

Patches do not live past a release on GitLab.com without an exception
granted by the delivery team.

Although patches are preserved during the omnibus upgrade, it is better if
they are incorporated into the next release so that they receive the same level
of testing and go through the normal release pipeline.

Patches can only be issued for rails and sidekiq, after a patch is applied
rails and sidekiq are issued a HUP signal. It is not possible at the time to
issue a patch for frontend code.

The following components do _not_ support post deployment patches:

* Front-end code or assets
* Gitaly
* Workhorse
* Nginx
* Postgresql
* Redis
* Registry


## Submitting a patch

Patches are initiated by the proposing backend team.

### Backend Developer

* Create a working branch in gitlab-ee from the current version running on
  production (visit https://gitlab.com/help to find out what this is).
  * For example, you can do `git checkout -b patch/my-fix v10.7.0-rc4-ee` if you want
    to derive your branch from `10.7.0 RC4` version based on the tag.
* Make your code changes
  * You can cherry-pick commits from CE / EE.
  * In that case remove changes to specs before generating the patch.
* Run the command `git --no-pager diff --color=never v10.7.0-rc4-ee.. -- app lib ee/app ee/lib > path/to/patch.patch`
  * **Note**: this is an example - if you have changed non-spec files in other
    directories, be sure to include those
* Create an MR in https://ops.gitlab.net/gitlab-com/gl-infra/patcher that adds
  the patch file(s) in a directory for the release that you want to patch. For
  example, to patch release `11.5.0-rc12` create a subdirectory for the release
  and copy the patch file(s) into https://ops.gitlab.net/gitlab-com/gl-infra/patcher/tree/master/patches/11.5.0-rc12-ee
* If there are already patch files for the release, simply add them to the
  existing directory, the patcher tool will make sure all patches are applied.
* Patches are applied in sorted order. To specify the order prefix the patch
  with a sort key such as:
    * `001-patch-for-issue-1.patch`
    * `002-patch-for-issue-2.patch`
* In the MR please link to the
  [corresponding incident issue](https://gitlab.com/gitlab-com/gl-infra/production/issues?label_name%5B%5D=incident)
  in the production tracker for the incident that requires the patch. If there
  isn't an existing incident issue, create one and ensure that is has the
  labels: `incident` and either `S1` or `S2` depending on the severity of hte
  problem.
* When the MR is submitted you should notify the oncall SRE of the change, if
  you are unsure who is oncall, send `/chatops run oncall prod` in the `#production`
  channel.

* Watch the pipeline to see a dryrun of the patch on staging. This will let you
  if there were any errors applying the patch.
* It is also possible to apply the patch to staging on a branch by manually initiating a
  pipeline step for one of the fleets. If this is necessary, please inform
  the oncall SRE engineer so it can be rolled back in the case the patch
  is not merged to master.
* When the patch is ready to merge, inform the oncall SRE by mentioning
  them in the `#releases` channel. The chatops command `/chatops run oncall prod`
  will display the current oncall engineer.

### OnCall SRE or Release Manager

* An SRE or Release Manager will merge the MR, this will automatically deploy the patch through
  the canary stage of the production environment.
* To monitor the process of the patch deployment see the
  [pipeline view of the patcher repository](https://ops.gitlab.net/gitlab-com/gl-infra/patcher/pipelines).
* When the patch is deployed to canary and verified, advance
  the patch to production by manually initiating the
  `Prepare-Production` stage.


#### Determining whether a patch will be applied

There are three factors determining whether a patch will be applied that are
checked prior to applying patches:

1. Is there a rails service running on the node being patched? This is looking
   for unicorn running with `gitlab-ctl status`.
1. Does the [patch release directory](https://ops.gitlab.net/gitlab-com/gl-infra/patcher/tree/master/files/patches)
   have an entry for the release that is running on the node being patched?
1. Is the patch a change? Before actually applying the patch it is applied in
   dry-run mode to see whether the patch would be applied.

This information is displayed in the output of the Ansible run in the following
task:

```
TASK [The following hosts will have the patch applied:] ************************
ok: [api-03-sv-gstg.c.gitlab-staging-1.internal] =>
  msg: FALSE | patch_dir_exists=True rails_exists=True patch_is_a_change=False
```

## Rolling back a patch

* If a patch needs to be rolled back rename the patch file to have a `.rollback` extension.
  For example if the patch file is named `patches/11.5.1-ee/profiles_helper.patch` rename
  the file on a branch to `patches/11.5.1-ee/profiles_helper.patch.rollback` with the same
  content.
* After the rollback file is created, contact a member of the delivery team to
  apply the rollback through the patcher pipeline.
